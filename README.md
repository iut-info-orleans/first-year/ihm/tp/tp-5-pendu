# Jeu du Pendu

## Pour compiler :
```bash
javac -d bin/ --module-path /path/to/javafx/lib --add-modules javafx.fxml,javafx.controls src/*.java
```

## Pour lancer le jeu :
```bash
java -cp bin --module-path /path/to/javafx/lib --add-modules javafx.fxml,javafx.controls Pendu
```
