public class Config {
  /**
   * Padding global pour les nodes
   */
  public final static Integer PADDING_GENERAL = 10;

  /**
   * Spacing général pour les boutons
   */
  public final static Integer SPACING_BOUTONS = 5;

  /**
   * Padding général pour l'intérieur des boutons
   */
  public final static Integer PADDING_BOUTONS = 10;
}
