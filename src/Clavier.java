import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.layout.TilePane;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Génère la vue d'un clavier et associe le contrôleur aux touches
 * le choix ici est d'un faire un héritié d'un TilePane
 */
public class Clavier extends TilePane {
    /**
     * Il est conseillé de stocker les touches dans un ArrayList
     */
    private List<Button> clavier;

    /**
     * Le contrôleur des touches
     */
    private EventHandler<ActionEvent> actionTouches;

    /**
     * Constructeur du clavier
     * @param actionTouches Le contrôleur des touches
     * @param tailleLigne Nombre de touches par ligne
     */
    public Clavier(EventHandler<ActionEvent> actionTouches, Integer nbTouchesParLigne) {
        this.clavier = new ArrayList<>();
        this.actionTouches = actionTouches;

        this.setDisposition();
        this.creerTouches();
        this.placerTouches(nbTouchesParLigne);
    }

    /**
     * Gère la disposition du TilePane
     */
    public void setDisposition() {
        this.setMaxWidth(350);
        this.setHgap(Config.SPACING_BOUTONS);
        this.setVgap(Config.SPACING_BOUTONS);
    }

    /**
     * Crée les différentes touches du clavier (De A à Z en majuscules)
     */
    private void creerTouches() {
        for (Integer codeAscii = 65; codeAscii < 91; codeAscii++) {
            Button lettre = new Button(Character.toString(codeAscii));

            lettre.setPadding(new Insets(Config.PADDING_BOUTONS));
            lettre.setOnAction(this.actionTouches);
            this.clavier.add(lettre);
        }
    }

    /**
     * Ajoute les touches au TilePane
     * @param nbTouchesParLigne Nombre maximum de touches sur une seule ligne
     */
    private void placerTouches(Integer nbTouchesParLigne) {
        this.setPrefColumns(nbTouchesParLigne);
        this.getChildren().addAll(this.clavier);
    }

    /**
     * Permet de désactiver certaines touches du clavier (et active les autres)
     * @param touchesDesactivees Une chaine de caractères contenant la liste des touches désactivées
     */
    public void desactiveTouches(Set<String> touchesDesactivees) {
        for (Button lettre: this.clavier) {
            if (touchesDesactivees.contains(lettre.getText()) && !lettre.isDisabled()) {
                lettre.setDisable(true);
            }
        }
    }

    /**
     * Réactive les touches du clavier à la fin d'une partie
     */
    public void reactiveTouches() {
        for (Button lettre: this.clavier) {
            lettre.setDisable(false);
        }
    }
}
