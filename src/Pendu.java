import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.stage.Stage;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import java.util.List;
import java.util.Arrays;
import java.io.File;
import java.util.ArrayList;

/**
 * Vue du jeu du pendu
 */
public class Pendu extends Application {
    /**
     * Modèle du jeu
     **/
    private MotMystere modelePendu;

    /**
     * Le nom de la page sur laquelle l'utilisateur se trouve (accueil, jeu...)
     */
    private String pageActuelle;

    /**
     * Liste qui contient les images du jeu
     */
    private ArrayList<Image> lesImages;

    /**
     * Liste qui contient les noms des niveaux
     */
    public List<String> niveaux;

    // les différents contrôles qui seront mis à jour ou consultés pour l'affichage
    /**
     * Le dessin du pendu
     */
    private ImageView dessin;

    /**
     * Le mot à trouver avec les lettres déjà trouvées
     */
    private Text motCrypte;

    /**
     * La barre de progression qui indique le nombre de tentatives
     */
    private ProgressBar pg;

    /**
     * Le clavier qui sera géré par une classe à implémenter
     */
    private Clavier clavier;

    /**
     * Le text qui indique le niveau de difficulté
     */
    private Text leNiveau;

    /**
     * Le chronomètre qui sera géré par une clasee à implémenter
     */
    private Chronometre chrono;

    /**
     * Le panel Central qui pourra être modifié selon le mode (accueil ou jeu)
     */
    private BorderPane panelCentral;

    /**
     * Le bouton Paramètre / Engrenage
     */
    private Button boutonParametres;

    /**
     * Le bouton Accueil / Maison
     */
    private Button boutonMaison;

    /**
     * Initialise les attributs (créer le modèle, charge les images, crée le chrono
     * ...)
     */
    @Override
    public void init() {
        this.modelePendu = new MotMystere("./mots/french", 3, 10, MotMystere.FACILE, 10);
        this.lesImages = new ArrayList<Image>();

        this.setPage("accueil");
        this.chargerImages("./img");

        this.dessin = new ImageView(this.lesImages.get(0));
        this.motCrypte = new Text();
        this.pg = new ProgressBar(0);
        this.clavier = new Clavier(new ControleurLettres(this, this.modelePendu), 8);
        this.leNiveau = new Text();
        this.chrono = new Chronometre();
    }

    /**
     * Retourne graphe de scène de la vue à partir de methodes précédantes
     */
    private Scene laScene() {
        this.panelCentral = new BorderPane();

        BorderPane fenetre = this.panelCentral;

        fenetre.setTop(this.banniere());

        return new Scene(fenetre, 800, 1000);
    }

    /**
     * Change le nom de la page sur laquelle l'utilisateur se trouve (accueil,
     * jeu...)
     */
    public void setPage(String nomPage) {
        this.pageActuelle = nomPage;
    }

    /**
     * Met à jourl'état des boutons de la bannière
     */
    public void majEtatBoutons() {
        this.boutonMaison.setDisable(this.pageActuelle.equals("accueil"));
        this.boutonParametres.setDisable(this.pageActuelle.equals("jeu"));
    }

    /**
     * Renvoie le mot crypté, avec les lettre déjà trouvées
     * 
     * @return Le mot crypté
     */
    public Text getMotCrypte() {
        return this.motCrypte;
    }

    /**
     * Renvoie l'image actuelle du pendu
     * 
     * @return L'image qui représente l'état actuel de la partie lancée
     */
    public ImageView getImagePendu() {
        return this.dessin;
    }

    /**
     * Renvoie la progressbar de la partie de pendu en cours
     * 
     * @return La progressbar
     */
    public ProgressBar getProgressBar() {
        return this.pg;
    }

    /**
     * Renvoie le clavier permettant de choisr les lettres
     * 
     * @return Le clavier
     */
    public Clavier getClavier() {
        return this.clavier;
    }

    /**
     * Renvoie le Text contenant le nom du niveau de la partie
     * 
     * @return Le Text avec le niveau de la partie
     */
    public Text getTextNiveau() {
        return this.leNiveau;
    }

    /**
     * Renvoie le chronomètre utilisé pour la page de jeu
     * 
     * @return Le chronomètre
     */
    public Chronometre getChronometre() {
        return this.chrono;
    }

    /**
     * @return La bannière contenant le titre du jeu et les boutons principaux
     */
    private BorderPane banniere() {
        BorderPane banniere = new BorderPane();
        Label titreJeu = new Label("Jeu du pendu");
        HBox hbBoutons = new HBox();

        banniere.setPadding(new Insets(20));
        banniere.setStyle("-fx-background-color: rgb(230, 230, 230);");
        BorderPane.setAlignment(titreJeu, Pos.CENTER);
        titreJeu.setStyle("-fx-font-size: 25px;");

        this.boutonMaison = new Button();
        this.boutonParametres = new Button();
        Button boutonInformations = new Button();

        ImageView imageMaison = new ImageView("home.png");
        ImageView imageParametres = new ImageView("parametres.png");
        ImageView imageInformations = new ImageView("info.png");

        for (ImageView image : Arrays.asList(imageMaison, imageParametres, imageInformations)) {
            image.setFitWidth(32);
            image.setPreserveRatio(true);
        }

        this.boutonMaison.setGraphic(imageMaison);
        this.boutonParametres.setGraphic(imageParametres);
        boutonInformations.setGraphic(imageInformations);

        this.boutonMaison.setOnAction(new RetourAccueil(this, this.modelePendu));
        boutonInformations.setOnAction(new ControleurInfos(this));

        hbBoutons.setSpacing(Config.SPACING_BOUTONS);
        hbBoutons.getChildren().addAll(this.boutonMaison, this.boutonParametres, boutonInformations);

        banniere.setLeft(titreJeu);
        banniere.setRight(hbBoutons);

        return banniere;
    }

    /**
     * Charge les images à afficher en fonction des erreurs
     * 
     * @param repertoire Répertoire où se trouvent les images
     */
    private void chargerImages(String repertoire) {
        for (int i = 0; i < this.modelePendu.getNbErreursMax() + 1; i++) {
            File file = new File(repertoire + "/pendu" + i + ".png");

            this.lesImages.add(new Image(file.toURI().toString()));
        }
    }

    /**
     * Affiche la page d'accueil
     */
    public void modeAccueil() {
        this.panelCentral.setCenter(new PageAccueil(this, this.modelePendu));
    }

    /**
     * Affiche la page de jeu
     */
    public void modeJeu() {
        this.panelCentral.setCenter(new PageJeu(this, this.modelePendu));
    }

    /**
     * 
     */
    public void modeParametres() {}

    /**
     * Raffraichit l'affichage selon les données du modèle
     */
    public void majAffichage() {
        this.motCrypte.setText(this.modelePendu.getMotCrypte());
        this.dessin.setImage(
                this.lesImages.get(this.modelePendu.getNbErreursMax() - this.modelePendu.getNbErreursRestants()));
        this.clavier.desactiveTouches(this.modelePendu.getLettresEssayees());
        this.pg.setProgress((this.modelePendu.getNbErreursMax() - this.modelePendu.getNbErreursRestants()) * 0.1);
        this.leNiveau
                .setText(String.format("Niveau %s", this.modelePendu.getNiveauString(this.modelePendu.getNiveau())));

        if (this.modelePendu.gagne()) {
            this.popUpMessageGagne().show();
            this.modeAccueil();
            this.setPage("accueil");
        }

        if (this.modelePendu.perdu()) {
            this.popUpMessagePerdu().show();
            this.modeAccueil();
            this.setPage("accueil");
        }
    }

    public Alert popUpPartieEnCours() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION,
                "La partie est en cours!\n Etes-vous sûr de l'interrompre ?", ButtonType.YES, ButtonType.NO);
        alert.setTitle("Attention");
        return alert;
    }

    public Alert popUpReglesDuJeu() {
        // A implementer
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        return alert;
    }

    public Alert popUpMessageGagne() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION,
                String.format("Vous avez gagné, le mot était bien : %s", this.modelePendu.getMotATrouve()));
        return alert;
    }

    public Alert popUpMessagePerdu() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION,
                String.format("Vous avez perdu... Le mot était : %s", this.modelePendu.getMotATrouve()));
        return alert;
    }

    /**
     * Créer le graphe de scène et lance le jeu
     * 
     * @param stage la fenêtre principale
     */
    @Override
    public void start(Stage stage) {
        stage.setTitle("IUTEAM'S - La plateforme de jeux de l'IUTO");
        stage.setScene(this.laScene());
        this.modeAccueil();
        stage.show();
    }

    /**
     * Programme principal
     * 
     * @param args inutilisé
     */
    public static void main(String[] args) {
        launch(args);
    }
}
