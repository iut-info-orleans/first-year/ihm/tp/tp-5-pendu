import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;

/**
 * Controleur du clavier
 */
public class ControleurLettres implements EventHandler<ActionEvent> {
    /**
     * Vue du jeu
     */
    private Pendu vuePendu;

    /**
     * Modèle du jeu
     */
    private MotMystere modelePendu;

    /**
     * @param modelePendu Modèle du jeu
     * @param vuePendu Vue du jeu
     */
    ControleurLettres(Pendu vuePendu, MotMystere modelePendu) {
        this.vuePendu = vuePendu;
        this.modelePendu = modelePendu;
    }

    /**
     * Actions à effectuer lors du clic sur une touche du clavier
     * Il faut donc: Essayer la lettre, mettre à jour l'affichage et vérifier si la partie est finie
     * @param actionEvent l'événement
     */
    @Override
    public void handle(ActionEvent actionEvent) {
        Button lettre = (Button)actionEvent.getTarget();

        this.modelePendu.essaiLettre(lettre.getText().charAt(0));
        this.vuePendu.majAffichage();
    }
}
