import java.util.ArrayList;
import java.util.List;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.text.Text;
import javafx.util.Duration;
import javafx.scene.text.TextAlignment;

/**
 * Permet de gérer un Text associé à une Timeline pour afficher un temps écoulé
 */
public class Chronometre extends Text {
    /**
     * le contrôleur associé au chronomètre
     */
    private ControleurChronometre actionTemps;

    /**
     * la fenêtre de temps
     */
    private KeyFrame keyFrame;

    /**
     * timeline qui va gérer le temps
     */
    private Timeline timeline;

    /**
     * Constructeur permettant de créer le chronomètre
     * avec un label initialisé à "0:0:0"
     * Ce constructeur créer la Timeline, la KeyFrame et le contrôleur
     */
    public Chronometre() {
        this.actionTemps = new ControleurChronometre(this);
        this.keyFrame = new KeyFrame(Duration.millis(1000), this.actionTemps);
        this.timeline = new Timeline(this.keyFrame);

        timeline.setCycleCount(Timeline.INDEFINITE);
        this.setDisposition();
    }

    /**
     * Gère la disposition du texte du chronomètre
     */
    private void setDisposition() {
        this.setTextAlignment(TextAlignment.CENTER);
        this.setStyle("-fx-font-size: 20px;");
    }

    /**
     * Permet au controleur de mettre à jour le text
     * la durée est affichée sous la forme m:s
     * 
     * @param tempsMillisec la durée depuis à afficher
     */
    public void setTime(long tempsMillisec) {
        this.setText(format(tempsMillisec));
    }

    /**
     * Permet de démarrer le chronomètre
     */
    public void start() {
        this.timeline.play();
    }

    /**
     * Permet d'arrêter le chronomètre
     */
    public void stop() {
        this.timeline.stop();
    }

    /**
     * Permet de remettre le chronomètre à 0
     */
    public void resetTime() {
        this.actionTemps.reset();
    }

    /**
     * Formatte la date comme ceci : 2h 17m 5s
     */
    public static String format(long time) {
        Long hours = ((time) / 60) / 60;
        Long minutes = (time / 60) % 60;
        Long seconds = time % 60;
        List<String> unites = new ArrayList<>();

        if (hours > 0) {
            unites.add(String.format("%dh", hours));
        }

        if (minutes > 0) {
            unites.add(String.format("%dm", minutes));
        }

        unites.add(String.format("%ds", seconds));

        return String.join(" ", unites);
    }
}
