import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ButtonType;
import java.util.Optional;

/**
 * Contrôleur à activer lorsque l'on clique sur le bouton Accueil
 */
public class RetourAccueil implements EventHandler<ActionEvent> {
    /**
     * Vue du jeu
     **/
    private Pendu vuePendu;

    /**
     * Modèle du jeu
     */
    private MotMystere modelePendu;

    /**
     * @param vuePendu vue du jeu
     * @param modelePendu modèle du jeu
     */
    public RetourAccueil(Pendu vuePendu, MotMystere modelePendu) {
        this.vuePendu = vuePendu;
        this.modelePendu = modelePendu;
    }


    /**
     * L'action consiste à retourner sur la page d'accueil. Il faut vérifier qu'il n'y avait pas une partie en cours
     * @param actionEvent l'événement action
     */
    @Override
    public void handle(ActionEvent actionEvent) {
         // On lance la fenêtre popup et on attend la réponse
         Optional<ButtonType> reponse = this.vuePendu.popUpPartieEnCours().showAndWait();

         // Si la réponse est oui
        if (reponse.isPresent() && reponse.get().equals(ButtonType.YES)) {
            this.modelePendu.setNiveau(MotMystere.FACILE);
            this.vuePendu.setPage("accueil");
            this.vuePendu.majEtatBoutons();
            this.vuePendu.getClavier().reactiveTouches();

            Chronometre chrono = this.vuePendu.getChronometre();

            chrono.stop();
            chrono.resetTime();
            this.vuePendu.modeAccueil();
        }
    }
}
