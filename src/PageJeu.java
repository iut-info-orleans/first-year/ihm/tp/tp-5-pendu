import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

public class PageJeu extends BorderPane {
  /**
   * La vue du jeu
   */
  private Pendu vuePendu;

  /**
   * Le modèle du pendu
   */
  private MotMystere modelePendu;

  /**
   * La partie Gauche du BorderPane
   */
  private VBox vbGauche;

  /**
   * La partie droite du BorderPane
   */
  private VBox vbDroite;

  /**
   * @param vuePendu    La vue du pendu
   * @param modelePendu Le modèle du pendu
   */
  public PageJeu(Pendu vuePendu, MotMystere modelePendu) {
    this.vuePendu = vuePendu;
    this.modelePendu = modelePendu;

    // Partie centrale du BorderPane
    this.vbGauche = new VBox();
    this.creerGauche();

    // Partie droite du BorderPane
    this.vbDroite = new VBox();
    this.creerDroite();

    this.setDisposition();
  }

  /**
   * Gère la disposition du BorderPane
   */
  private void setDisposition() {
    this.setPadding(new Insets(Config.PADDING_GENERAL));
    this.vbGauche.setAlignment(Pos.TOP_CENTER);
    this.vbGauche.setSpacing(Config.PADDING_GENERAL);
    this.vbDroite.setSpacing(20);
  }


  /**
   * Gère l'affichage de la partie Gauche du BorderPane
   */
  private void creerGauche() {
    VBox vbGauche = this.vbGauche;
    Pendu vuePendu = this.vuePendu;

    vbGauche.getChildren().addAll(vuePendu.getMotCrypte(), vuePendu.getImagePendu(), vuePendu.getProgressBar(),
        vuePendu.getClavier());
    this.setLeft(vbGauche);
  }

  /**
   * Gère l'affichage de la partie Gauche du BorderPane
   */
  private void creerDroite() {
    VBox vbDroite = this.vbDroite;
    Pendu vuePendu = this.vuePendu;
    Text niveau = vuePendu.getTextNiveau();
    TitledPane chrono = new TitledPane("Chronomètre", vuePendu.getChronometre());
    Button nouveauMot = new Button("Nouveau mot");

    niveau.setStyle("-fx-font-size: 25px;");
    nouveauMot.setOnAction(new ControleurLancerPartie(this.vuePendu, this.modelePendu));

    vbDroite.setSpacing(20);
    vbDroite.getChildren().addAll(niveau, chrono, nouveauMot);
    this.setRight(vbDroite);
  }
}
