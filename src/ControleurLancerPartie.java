import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ButtonType;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

/**
 * Contrôleur à activer lorsque l'on clique sur le bouton rejouer ou Lancer une
 * partie
 */
public class ControleurLancerPartie implements EventHandler<ActionEvent> {
    /**
     * vue du jeu
     **/
    private Pendu vuePendu;

    /**
     * modèle du jeu
     */
    private MotMystere modelePendu;

    /**
     * @param vuePendu    Vue du jeu
     * @param modelePendu Modèle du jeu
     */
    public ControleurLancerPartie(Pendu vuePendu, MotMystere modelePendu) {
        this.vuePendu = vuePendu;
        this.modelePendu = modelePendu;
    }

    /**
     * L'action consiste à recommencer une partie. Il faut vérifier qu'il n'y a pas
     * une partie en cours
     * 
     * @param actionEvent l'événement action
     */
    @Override
    public void handle(ActionEvent actionEvent) {
        // On lance la fenêtre popup et on attend la réponse
        Optional<ButtonType> reponse = this.vuePendu.popUpPartieEnCours().showAndWait();

        // Si la réponse est oui
        if (reponse.isPresent() && reponse.get().equals(ButtonType.YES)) {
            this.vuePendu.setPage("jeu");
            this.vuePendu.majEtatBoutons();
            this.modelePendu.setMotATrouver();
            this.vuePendu.majAffichage();
            this.vuePendu.getClavier().reactiveTouches();

            Chronometre chrono = this.vuePendu.getChronometre();

            chrono.resetTime();
            chrono.start();
            this.vuePendu.modeJeu();

            String motATrouver = this.modelePendu.getMotATrouve();

            // Pour le mode facile, on masque les touches des lettres qui ne sont pas dans le mot
            if (this.modelePendu.getNiveau() == MotMystere.FACILE) {
                Set<String> touchesADesactiver = new HashSet<>();

                for (Integer codeAscii = 65; codeAscii < 91; codeAscii++) {
                    String lettre = Character.toString(codeAscii);

                    if (!motATrouver.contains(lettre)) {
                        touchesADesactiver.add(lettre);
                    }
                }

                this.vuePendu.getClavier().desactiveTouches(touchesADesactiver);
            }

            System.out.println(String.format("Le mot à trouver est : %s", motATrouver));
        }
    }
}
