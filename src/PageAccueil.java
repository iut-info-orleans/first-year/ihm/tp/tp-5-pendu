import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TitledPane;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.VBox;

public class PageAccueil extends VBox {
  private Pendu vuePendu;
  private MotMystere modelePendu;


  /**
   * @param vuePendu Vue du jeu
   * @param modelePendu Modèle du jeu
   */
  public PageAccueil(Pendu vuePendu, MotMystere modelePendu) {
    this.vuePendu = vuePendu;
    this.modelePendu = modelePendu;

    this.setDisposition();
    this.ajouteBoutonPartie();
    this.ajouteRadioBoutons();
  }

  /**
   * Gère la disposition du Pane
   */
  private void setDisposition() {
    this.setPadding(new Insets(Config.PADDING_GENERAL));
    this.setSpacing(Config.PADDING_GENERAL);
  }

  /**
   * Ajoute le bouton qui permet de commencer une partie
   */
  private void ajouteBoutonPartie() {
    Button boutonJouer = new Button("Lancer la partie");

    boutonJouer.setOnAction(new ControleurLancerPartie(this.vuePendu, this.modelePendu));
    this.getChildren().add(boutonJouer);
  }

  /**
   * Ajoute les raio boutons qui permettent de choisir le niveau de difficulté
   * d'une partie
   */
  private void ajouteRadioBoutons() {
    VBox vbRadioBoutons = new VBox();
    ToggleGroup groupeBoutonsRadio = new ToggleGroup();
    Boolean radioButtonDefault = false;

    vbRadioBoutons.setSpacing(Config.SPACING_BOUTONS);

    for (Integer niveau = 0; niveau < 4; niveau++) {
      String difficulte = this.modelePendu.getNiveauString(niveau);
      RadioButton choixNiveau = new RadioButton(difficulte);

      choixNiveau.setOnAction(new ControleurNiveau(this.modelePendu));
      choixNiveau.setToggleGroup(groupeBoutonsRadio);

      if (!radioButtonDefault) {
        choixNiveau.setSelected(true);
        radioButtonDefault = true;
      }

      vbRadioBoutons.getChildren().add(choixNiveau);
    }

    TitledPane sectionRadioBoutons = new TitledPane("Niveau de difficulté", vbRadioBoutons);

    this.getChildren().add(sectionRadioBoutons);
  }
}
